@numchapentry{General Information: README}{1}{General Information}{1}
@numsecentry{License}{1.1}{License}{1}
@numsecentry{Installation}{1.2}{Installation}{1}
@numsecentry{Installing from a third-party repository}{1.3}{Installing from a third-party repository}{2}
@numsecentry{Installing from source}{1.4}{Installing from source}{2}
@numsecentry{Activating and Loading ESS}{1.5}{Activating and Loading ESS}{2}
@numsecentry{Check Installation}{1.6}{Check Installation}{3}
@numsecentry{Starting an ESS process}{1.7}{Starting up}{3}
@numsecentry{Current Features}{1.8}{Current Features}{3}
@numsecentry{New Features}{1.9}{New Features}{4}
@numsecentry{Reporting Bugs}{1.10}{Reporting Bugs}{8}
@numsecentry{Mailing Lists}{1.11}{Mailing Lists}{9}
@numsecentry{Authors}{1.12}{Authors}{9}

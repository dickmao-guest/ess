@numchapentry{Introduction to ESS}{1}{Introduction}{1}
@numsecentry{Why should I use ESS?}{1.1}{Features}{1}
@numsubsecentry{Features Overview}{1.1.1}{Current Features}{2}
@numsecentry{New features in ESS}{1.2}{New features}{3}
@numsecentry{Authors of and contributors to ESS}{1.3}{Credits}{8}
@numsecentry{How to read this manual}{1.4}{Manual}{9}
@numchapentry{Installing ESS on your system}{2}{Installation}{11}
@numsecentry{Installing from a third-party repository}{2.1}{Installing from a third-party repository}{11}
@numsecentry{Installing from source}{2.2}{Installing from source}{11}
@numsecentry{Activating and Loading ESS}{2.3}{Activating and Loading ESS}{12}
@numsecentry{Check Installation}{2.4}{Check Installation}{12}
@numchapentry{Interacting with statistical programs}{3}{Interactive ESS}{13}
@numsecentry{Starting an ESS process}{3.1}{Starting up}{13}
@numsecentry{Running more than one ESS process}{3.2}{Multiple ESS processes}{13}
@numsecentry{ESS processes on Remote Computers}{3.3}{ESS processes on Remote Computers}{13}
@numsubsecentry{ESS and TRAMP}{3.3.1}{}{13}
@numsubsecentry{ESS-remote}{3.3.2}{}{14}
@numsecentry{Changing the startup actions}{3.4}{Customizing startup}{15}
@numchapentry{Interacting with the ESS process}{4}{Entering commands}{16}
@numsecentry{Entering commands and fixing mistakes}{4.1}{Command-line editing}{16}
@numsecentry{Manipulating the transcript}{4.2}{Transcript}{16}
@numsubsecentry{Manipulating the output from the last command}{4.2.1}{Last command}{17}
@numsubsecentry{Viewing older commands}{4.2.2}{Process buffer motion}{17}
@numsubsecentry{Re-submitting commands from the transcript}{4.2.3}{Transcript resubmit}{17}
@numsubsecentry{Keeping a record of your R session}{4.2.4}{Saving transcripts}{18}
@numsecentry{Command History}{4.3}{Command History}{19}
@numsubsecentry{Saving the command history}{4.3.1}{Saving History}{20}
@numsecentry{References to historical commands}{4.4}{History expansion}{20}
@numsecentry{Hot keys for common commands}{4.5}{Hot keys}{21}
@numsecentry{Is the Statistical Process running under ESS?}{4.6}{Statistical Process running in ESS?}{23}
@numsecentry{Using emacsclient}{4.7}{Emacsclient}{23}
@numsecentry{Other commands provided by inferior-ESS}{4.8}{Other}{23}
@numchapentry{Sending code to the ESS process}{5}{Evaluating code}{25}
@numchapentry{Manipulating saved transcript files}{6}{Transcript Mode}{27}
@numsecentry{Resubmitting commands from the transcript file}{6.1}{Resubmit}{27}
@numsecentry{Cleaning transcript files}{6.2}{Clean}{27}
@numchapentry{Editing objects and functions}{7}{Editing objects}{28}
@numsecentry{Creating or modifying R objects}{7.1}{Edit buffer}{28}
@numsecentry{Loading source files into the ESS process}{7.2}{Loading}{28}
@numsecentry{Detecting errors in source files}{7.3}{Error Checking}{29}
@numsecentry{Indenting and formatting R code}{7.4}{Indenting}{29}
@numsubsecentry{Changing styles for code indentation and alignment}{7.4.1}{Styles}{30}
@numsecentry{Commands for motion, completion and more}{7.5}{Other edit buffer commands}{31}
@numsecentry{Maintaining R source files}{7.6}{Source Files}{31}
@numsecentry{Names and locations of dump files}{7.7}{Source Directories}{33}
@numchapentry{Reading help files}{8}{Help}{35}
@numchapentry{Completion}{9}{Completion}{37}
@numsecentry{Completion of object names}{9.1}{Object names}{37}
@numsecentry{Completion of function arguments}{9.2}{Function arguments}{37}
@numsecentry{Minibuffer completion}{9.3}{Minibuffer completion}{38}
@numsecentry{Integration with auto-complete package}{9.4}{Auto-complete}{38}
@numsecentry{Company}{9.5}{Company}{38}
@numsecentry{Icicles}{9.6}{Icicles}{38}
@numchapentry{Developing with ESS}{10}{Developing with ESS}{40}
@numsecentry{ESS tracebug}{10.1}{ESS tracebug}{40}
@numsubsecentry{Getting started with tracebug}{10.1.1}{Getting started with tracebug}{41}
@numsecentry{Editing documentation}{10.2}{Editing documentation}{42}
@numsubsecentry{Editing R documentation (Rd) files}{10.2.1}{R documentation files}{42}
@numsubsecentry{Editing Roxygen documentation}{10.2.2}{Roxygen}{43}
@numsecentry{Namespaced Evaluation}{10.3}{Namespaced Evaluation}{45}
@numchapentry{Other ESS features and tools}{11}{Extras}{46}
@numsecentry{ElDoc}{11.1}{ESS ElDoc}{46}
@numsecentry{Flymake}{11.2}{ESS Flymake}{46}
@numsecentry{Handy commands}{11.3}{Handy commands}{47}
@numsecentry{Syntactic highlighting of buffers}{11.4}{Highlighting}{47}
@numsecentry{Parenthesis matching}{11.5}{Parens}{48}
@numsecentry{Using graphics with ESS}{11.6}{Graphics}{48}
@numsubsecentry{Using ESS with the @code {printer()} driver}{11.6.1}{printer}{48}
@numsubsecentry{Using ESS with windowing devices}{11.6.2}{X11}{48}
@numsubsecentry{Java Graphics Device}{11.6.3}{winjava}{48}
@numsecentry{Imenu}{11.7}{Imenu}{48}
@numsecentry{Toolbar}{11.8}{Toolbar}{49}
@numsecentry{Xref}{11.9}{Xref}{49}
@numsecentry{Rdired}{11.10}{Rdired}{49}
@numsecentry{Rutils}{11.11}{Rutils}{49}
@numsecentry{Interaction with Org mode}{11.12}{Org}{51}
@numsecentry{Support for Sweave in ESS and AUCTeX}{11.13}{Sweave and AUCTeX}{51}
@numchapentry{Overview of ESS features for the S family}{12}{ESS for R}{52}
@numsecentry{ESS[R]--Editing files}{12.1}{ESS(R)--Editing files}{52}
@numsecentry{iESS[R]--Inferior ESS processes}{12.2}{iESS(R)--Inferior ESS processes}{52}
@numsecentry{Philosophies for using ESS[R]}{12.3}{Philosophies for using ESS(R)}{53}
@numsecentry{Example ESS usage}{12.4}{Example ESS usage}{54}
@numchapentry{ESS for SAS}{13}{ESS for SAS}{56}
@numsecentry{ESS[SAS]--Design philosophy}{13.1}{ESS(SAS)--Design philosophy}{56}
@numsecentry{ESS[SAS]--Editing files}{13.2}{ESS(SAS)--Editing files}{56}
@numsecentry{ESS[SAS]--@key {TAB} key}{13.3}{ESS(SAS)--TAB key}{57}
@numsecentry{ESS[SAS]--Batch SAS processes}{13.4}{ESS(SAS)--Batch SAS processes}{57}
@numsecentry{ESS[SAS]--Function keys for batch processing}{13.5}{ESS(SAS)--Function keys for batch processing}{59}
@numsecentry{iESS[SAS]--Interactive SAS processes}{13.6}{iESS(SAS)--Interactive SAS processes}{62}
@numsecentry{iESS[SAS]--Common problems}{13.7}{iESS(SAS)--Common problems}{63}
@numsecentry{ESS[SAS]--Graphics}{13.8}{ESS(SAS)--Graphics}{64}
@numsecentry{ESS[SAS]--Windows}{13.9}{ESS(SAS)--Windows}{64}
@numchapentry{ESS for BUGS}{14}{ESS for BUGS}{65}
@numsecentry{ESS[BUGS]--Model files}{14.1}{}{65}
@numsecentry{ESS[BUGS]--Command files}{14.2}{}{65}
@numsecentry{ESS[BUGS]--Log files}{14.3}{}{65}
@numchapentry{ESS for JAGS}{15}{ESS for JAGS}{66}
@numsecentry{ESS[JAGS]--Model files}{15.1}{}{66}
@numsecentry{ESS[JAGS]--Command files}{15.2}{}{66}
@numsecentry{ESS[JAGS]--Log files}{15.3}{}{66}
@numchapentry{Bugs and Bug Reporting, Mailing Lists}{16}{Mailing lists/bug reports}{67}
@numsecentry{Bugs}{16.1}{Bugs}{67}
@numsecentry{Reporting Bugs}{16.2}{Reporting Bugs}{67}
@numsecentry{Mailing Lists}{16.3}{Mailing Lists}{67}
@numsecentry{Help with Emacs}{16.4}{Help with Emacs}{68}
@appentry{Customizing ESS}{A}{Customization}{69}
@unnchapentry{Indices}{10001}{Indices}{70}
@unnsecentry{Key index}{10001.1}{Key index}{70}
@unnsecentry{Function and program index}{10001.2}{Function and program index}{70}
@unnsecentry{Variable index}{10001.3}{Variable index}{72}
@unnsecentry{Concept Index}{10001.4}{Concept index}{72}
